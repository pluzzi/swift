﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OPCPanel.Models
{
    public class Service
    {
        private string _name;
        private string _status;
        private string _sqlConnStatus;
        private string _opcConnStatus;
        private string _nodeId;

        public string Name { get; set; }
        public string Status { get; set; }
        public string SqlConnStatus { get; set; }
        public string OpcConnStatus { get; set; }
        public string NodeId { get; set; }

        public Service() { }

        public Service(int id)
        {
            // MOCK
            var rand = new Random();
            var i = rand.Next(101);
            var io = rand.Next(2);

            Name = "ServicioOPC" + id.ToString();
            Status = io == 1 ? "OK" : "ERROR";
            SqlConnStatus = io == 1 ? "OK" : "ERROR";
            OpcConnStatus = io == 1 ? "OK" : "ERROR";
            NodeId = "NodeId" + i.ToString().PadLeft(4, '0');
        }

    }
}
