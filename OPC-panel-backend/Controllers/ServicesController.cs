﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OPCPanel.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OPCPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ServicesController : ControllerBase
    {
        // GET: api/<ServicesController>
        [HttpGet]
        public IEnumerable<Service> Get()
        {
            Service[] services = new Service[] {
                new Service(1),
                new Service(2),
                new Service(3),
                new Service(4),
                new Service(5),
                new Service(6)
            };

            return services;

        }
    }
}
