import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message: string;
  form: FormGroup;
  
  constructor(
    private loginSrv: LoginService,
    private userService: UsersService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({          
      'user': new FormControl(null),
      'password': new FormControl(null)
    });
  }

  login(){
    console.log(this.form);
    this.loginSrv.login(this.form.controls['user'].value, this.form.controls['password'].value).subscribe(
      result => {
        debugger
        if(result.token != undefined) {
          let usr = {
            user: this.form.controls['user'].value,
            password: this.form.controls['password'].value,
            token: result.token
          }
          this.userService.setUser(usr);
          this.router.navigate(['home'], {  });
        }else{
          this.message = 'El usuario o la contraseña no son correctos.';
        }
      },
      error => {
        this.message = 'El usuario o la contraseña no son correctos.';
      }
    )
  }

}
