import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { ServicesService } from 'src/app/services/services.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  services: any[];

  constructor(
    private servicesSrv: ServicesService,
    private userSrv: UsersService,
    private loginSrv: LoginService
  ) { }

  ngOnInit(): void {
    this.getServices();
    setInterval(() => {
      this.getServices(); 
    }, 5000);
  }

  getServices(){
    this.servicesSrv.getServices().subscribe(
      result => {
        this.services = result;
      },
      error => {
        const user = this.userSrv.getUser();
        this.loginSrv.login(user.user, user.password).subscribe(
          result => {
            if(result.token != undefined) {
              let newUser = {
                user: user.user,
                password: user.password,
                token: result.token
              }
              this.userSrv.setUser(newUser);
            }
          }
        )
      }
    )
  }
 

}
