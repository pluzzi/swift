import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(
    private userService: UsersService,
  ) { }

  getBaseUrl(){
    return environment.apiUrl;
  }

  getHeaders(){
    var headers = new HttpHeaders();
    const user = this.userService.getUser();

    if(user){
      const token = user.token;
      headers = headers.append('Authorization', `Bearer ${token}`);
    }

    return headers;
  }
}
