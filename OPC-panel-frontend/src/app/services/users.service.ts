import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private user: any;

  constructor(
    private router: Router
  ) { }

  setUser(user: any): void {
    debugger
    this.user = user;
    
    if(!this.user){
      localStorage.removeItem('user');
    }else{
      localStorage.setItem('user', JSON.stringify(user));
    }
  }

  getUser(): any {
    if(this.user){
      return this.user;
    }  else{
      return JSON.parse(localStorage.getItem('user'));
    }
  }

  isLogin(){
    if( this.router.url !== '/' ){
      if ( this.getUser() === null ){
        this.router.navigate(['/'], {  });
      }else{
        this.router.navigate([this.router.url], {  });
      }
    }
  }
}
