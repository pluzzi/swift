import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  getServices(): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + 'services',
      { headers: this.config.getHeaders() }
    );
  }
}
