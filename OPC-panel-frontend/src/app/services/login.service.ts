import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  login(user: string, password: string): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + 'login/' + user + '/' + password,
    );
  }

  

}
