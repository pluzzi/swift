import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from './../environments/environment';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private router: Router,
    private userSrv: UsersService
  ) {
    console.log(environment);
    router.events.subscribe((val) => {
      this.userSrv.isLogin();
    });
  }
}
